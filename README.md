# Skee-Ball
A skee-ball simulator with multiple holes played on a table

---
## Latest Build
[Android v0.1][apklink]

---
## Development
### Supported Platform
Android: API 18 or above, OpenGL 3.0 or above

### Tools
Unity3D (2019.2.14f1)

---
## Game Configuration
Each game scene (game level) should have a game controller, should be under "GameController" GameObject

 - Power Cycle: time(sec) for power bar move from 0 to 100% or reverse
 - Total Game Time: time(sec) for a single round game
 - Distance Can Reshoot: the distance which player can shoot ball again after the shot ball fly away from camera
 - Angle To Shoot: angle to shoot a ball, 0 shoot horizontally, 90 shoot vertically
 - Power To Shoot: impulse force added to the ball while shooting
 - Power To Shoot Variance: maximum extra impulse force added to the ball while shooting
 - General Mass Of Ball: mass of a normal ball
 - Mass Variance: extra mass of a normall ball (mass of ball = General Mass Of Ball ± Mass Variance)

[apklink]: <https://bitbucket.org/autoktk/skeeball/downloads/android_v0.1.apk>