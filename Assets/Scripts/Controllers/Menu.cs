﻿using Datas;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Controllers
{
    public class Menu : MonoBehaviour
    {
        [Header("Setup")]
        [SerializeField]
        protected Views.Menu uiMenu;
        [SerializeField]
        protected Views.Game uiGame;

        [Header("Hidden")]
        private Scene gameScene;

        private void Awake()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            uiMenu.OnStartClicked.RemoveListener(OnStartClicked);
            uiMenu.OnStartClicked.AddListener(OnStartClicked);
            uiGame.OnGoBackClicked.RemoveListener(OnBackClicked);
            uiGame.OnGoBackClicked.AddListener(OnBackClicked);
            uiGame.OnTryAgainClicked.RemoveListener(OnTryAgainClicked);
            uiGame.OnTryAgainClicked.AddListener(OnTryAgainClicked);
        }

        private void OnDestroy()
        {
            uiMenu.OnStartClicked.RemoveListener(OnStartClicked);
            uiGame.OnGoBackClicked.RemoveListener(OnBackClicked);
            uiGame.OnTryAgainClicked.RemoveListener(OnTryAgainClicked);
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        private void OnStartClicked()
        {
            uiMenu.ShowLoading(true);
            StartCoroutine(LoadScene(Constant.GAME_SCENE_1, () =>
            {
                uiMenu.ShowLoading(false);
                uiGame.Show(true);
                uiMenu.Show(false);
            }));
        }

        private void OnTryAgainClicked()
        {
            uiMenu.ShowLoading(true);
            uiGame.Show(false);
            StartCoroutine(ReloadScene(gameScene, () =>
            {
                uiMenu.ShowLoading(false);
                uiGame.Show(true);
            }));
        }

        private void OnBackClicked()
        {
            uiMenu.ShowLoading(true);
            uiGame.Show(false);
            StartCoroutine(UnLoadScene(gameScene, () =>
            {
                gameScene = default(Scene);
                uiMenu.ShowLoading(false);
                uiMenu.Show(true);
            }));
        }

        /* 
         * @desc call when scene manager load scene async finished
         * @param scene - scene being loaded
         * @param loadSceneMode - type of scene being loaded (expected to be LoadSceneMode.Additive in this class)
         */
        private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            if (loadSceneMode == LoadSceneMode.Additive)
            {
                gameScene = scene;
            }
        }

        private IEnumerator LoadScene(string sceneName, System.Action callback)
        {
            var asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            // can add loading progress later
            yield return asyncOperation;
            callback.Invoke();

        }

        private IEnumerator UnLoadScene(Scene scene, System.Action callback)
        {
            var asyncOperation = SceneManager.UnloadSceneAsync(scene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            // can add loading progress later
            yield return asyncOperation;
            yield return Resources.UnloadUnusedAssets();
            callback.Invoke();
        }

        private IEnumerator ReloadScene(Scene scene, System.Action callback)
        {
            string sceneName = gameScene.name;
            var asyncOperation = SceneManager.UnloadSceneAsync(scene, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            // can add loading progress later
            yield return asyncOperation;
            asyncOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            yield return asyncOperation;
            callback.Invoke();
        }
    }
}
