﻿using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Controllers
{
    public class Bucket : MonoBehaviour
    {
        [Header("Setup")]
        [SerializeField]
        protected Text labelScore;
        public int score 
        { 
            get 
            {
                if (int.TryParse(labelScore.text, out int value))
                {
                    return value;
                }
                return 0;
            }
            set
            {
                labelScore.text = value.ToString();
            }
        }
        public BucketEvent OnDetectBall;

        public void OnScorerDetectBall(MonobehaviorEventTransfer caller, Collider other)
        {
            if (other != null)
            {
                Ball ball = other.GetComponent<Ball>();
                if (ball != null)
                {
                    //Debug.LogFormat("detected: {0}, {1}", caller, other);
                    OnDetectBall.Invoke(this, ball);
                }
            }
        }
    }

    [System.Serializable]
    public class BucketEvent : UnityEvent<Bucket, Ball>
    {

    }
}
