﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controllers
{
    public class Game : MonoBehaviour
    {
        [Header("Resources")]
        [SerializeField]
        protected Ball prefabBall;

        [Header("Setup")]
        [SerializeField]
        protected Transform tranCameraPosition;
        [SerializeField]
        protected Bucket[] buckets;
        [SerializeField]
        protected Transform tranShooter;
        private Views.Game ui;

        [Header("Config")]
        [Tooltip("Time(sec) for power from 0 to 100%")]
        [Range(0.1f, 10)]
        [SerializeField]
        protected float powerCycle;
        [Range(5, 60)]
        [SerializeField]
        protected float totalGameTime;
        [SerializeField]
        [Tooltip("Distance of ball away from camera which player can shoot the next")]
        [Range(0, 10)]
        protected float distanceCanReshoot;
        [Tooltip("Angle to shoot ball, 0 shoot horizontally, 90 shoot vertically")]
        [Range(0, 90)]
        [SerializeField]
        protected float angleToShoot;
        [Tooltip("Min power to shoot ball")]
        [Range(0, 90)]
        [SerializeField]
        protected float powerToShoot;
        [Tooltip("Power bar max power added to min speed")]
        [Range(0, 90)]
        [SerializeField]
        protected float powerToShootVariance;
        [Range(0.01f, 1f)]
        [SerializeField]
        protected float generalMassOfBall;
        [Range(0, 1f)]
        [SerializeField]
        protected float massVariance;


        [Header("Private")]
        private Status status;
        private int layerShooter;
        private Ball currentBall;
        private float currentPowerTime;
        private float currentPowerDirection;
        private float currentGameTime;
        private int score;
        private List<int> listScoreMultiplePool;
        private float distanceCamereShooter;

        private void Awake()
        {
            layerShooter = 1 << LayerMask.NameToLayer(Datas.Constant.LAYER_NAME_SHOOTER);
            var tranCam = Camera.main.transform;
            tranCam.SetPositionAndRotation(tranCameraPosition.position, tranCameraPosition.rotation);
            distanceCamereShooter = Vector3.Distance(tranCameraPosition.position, tranShooter.position);
        }

        private void OnDestroy()
        {
            ui.OnTouchDown.RemoveListener(OnTouchDown);
            ui.OnTouchUp.RemoveListener(OnTouchUp);
            ui.OnTouchDrag.RemoveListener(OnTouchDrag);
        }


        void Start()
        {
            ui = FindObjectOfType<Views.Game>();
            ui.OnTouchDown.RemoveListener(OnTouchDown);
            ui.OnTouchDown.AddListener(OnTouchDown);
            ui.OnTouchUp.RemoveListener(OnTouchUp);
            ui.OnTouchUp.AddListener(OnTouchUp);
            ui.OnTouchDrag.RemoveListener(OnTouchDrag);
            ui.OnTouchDrag.AddListener(OnTouchDrag);
            currentGameTime = 3;
            score = 0;
            status = Status.CountDown;
            ui.SetGameTime(0);
            ui.ClearScore();
        }

        // Update is called once per frame
        void Update()
        {
            switch (status)
            {
                case Status.CountDown:
                    currentGameTime -= Time.deltaTime;
                    if (currentGameTime > 0)
                    {
                        ui.ShowCountDown(currentGameTime);
                    }
                    else
                    {
                        ui.UnshowCountDown();
                        currentGameTime = totalGameTime;
                        status = Status.Playing;
                    }
                    break;
                case Status.Playing:
                    currentGameTime -= Time.deltaTime;
                    if (currentGameTime < 0)
                    {
                        status = Status.Finished;
                        if (currentBall != null && currentBall.isHolding)
                        {
                            Destroy(currentBall.gameObject);
                        }
                        ui.UnshowPower();
                        ui.ShowGameOver(score, Datas.Leaderboard.AddScore(score));
                    }
                    else
                    {
                        if (currentBall != null)
                        {
                            if (currentBall.isHolding)
                            {
                                currentPowerTime += Time.deltaTime * currentPowerDirection;
                                if (currentPowerTime > powerCycle)
                                {
                                    currentPowerTime = (powerCycle - currentPowerTime) + powerCycle;
                                    currentPowerDirection = -1;
                                }
                                else if (currentPowerTime < 0)
                                {
                                    currentPowerTime = Mathf.Abs(currentPowerTime);
                                    currentPowerDirection = 1;
                                }
                                ui.ShowPower(PowerConvertor(currentPowerTime/powerCycle));
                            }
                            else
                            {
                                if (Vector3.Distance(currentBall.transform.position, Camera.main.transform.position) > distanceCanReshoot + distanceCamereShooter)
                                {
                                    currentBall = null;
                                }
                            }
                        }
                        ui.SetGameTime(currentGameTime);
                    }
                    break;
            }
        }

        /**
         * call from bucket scorer when ball is fall inside the bucket
         */
        public void OnBucketDetectBall(Bucket bucket, Ball ball)
        {
            if (status == Status.Playing)
            {
                score += bucket.score * ball.scoreMultipler;
                ui.SetScore(score);
            }
            ball.gameObject.SetActive(false);
        }

        private void OnTouchDown(Ray ray)
        {
            if (status != Status.Playing)
                return;
            if (currentBall == null)
            {
                if (Physics.Raycast(ray, out RaycastHit hitInfo, float.MaxValue, layerShooter))
                {
                    currentBall = Ball.Instantiate<Ball>(prefabBall);
                    currentBall.mass = generalMassOfBall + Random.Range(-massVariance, massVariance);
                    currentBall.isHolding = true;
                    currentBall.transform.position = hitInfo.point;
                    currentBall.scoreMultipler = GetScoreMultipler();
                    currentPowerDirection = 1;
                    ui.ShowPower(currentPowerTime=0);
                }
            }
        }
        private void OnTouchUp(Ray ray)
        {
            if (status != Status.Playing)
                return;
            if (currentBall != null && currentBall.isHolding)
            {
                if (Physics.Raycast(ray, out RaycastHit hitInfo, float.MaxValue, layerShooter))
                {   
                    currentBall.transform.position = hitInfo.point;
                    currentBall.isHolding = false;
                    Vector3 direction = Quaternion.Lerp(Quaternion.Euler(tranShooter.forward), Quaternion.Euler(tranShooter.up), angleToShoot / 90f).eulerAngles;
                    currentBall.ShootAt(direction * (powerToShoot + PowerConvertor(currentPowerTime / powerCycle) * powerToShootVariance));
                    ui.UnshowPower();
                    Destroy(currentBall.gameObject, 5);
                }
            }
        }

        private void OnTouchDrag(Ray ray)
        {
            if (status != Status.Playing)
                return;
            if (currentBall != null && currentBall.isHolding)
            {
                if (Physics.Raycast(ray, out RaycastHit hitInfo, float.MaxValue, layerShooter))
                {
                    currentBall.transform.position = hitInfo.point;
                }
            }
        }

        /*
         * @desc convert a linear progress power bar to some other progress, currently using power law
         * @param float percent: 0-1
         * @return float: 0-1
         */
        private float PowerConvertor(float percent)
        {
            return Mathf.Pow(percent, 2f);
        }

        /*
         * generate a score multipler for ball, so different ball may have a bonus score
         */
        private int GetScoreMultipler()
        {
            if (listScoreMultiplePool == null)
            {
                listScoreMultiplePool = new List<int>();
            }
            if (listScoreMultiplePool.Count <= 0)
            {
                while (listScoreMultiplePool.Count < Datas.Constant.SCORE_MULTIPLER_POOL_X2)
                {
                    listScoreMultiplePool.Add(2);
                }
                while (listScoreMultiplePool.Count < Datas.Constant.SCORE_MULTIPLER_POOL_SIZE)
                {
                    listScoreMultiplePool.Add(1);
                }
            }
            int index = Random.Range(0, listScoreMultiplePool.Count);
            int value = listScoreMultiplePool[index];
            listScoreMultiplePool.RemoveAt(index);
            return value;
        }

        enum Status
        {
            CountDown,
            Playing,
            Finished,
        }
    }
}
