﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Controllers
{
    public class Ball : MonoBehaviour
    {
        [Header("Setup")]
        [SerializeField]
        protected Rigidbody body;
        private bool _isHolding;
        public bool isHolding
        {
            get { return _isHolding; }
            set
            {
                _isHolding = value;
                body.isKinematic = _isHolding;
                body.useGravity = !_isHolding;
            }
        }
        public float mass
        {
            get
            {
                return body.mass;
            }
            set
            {
                body.mass = value;
            }
        }
        public int scoreMultipler = 1;

        public void ShootAt(Vector3 impulseForce)
        {
            body.AddForce(impulseForce, ForceMode.Impulse);
        }
    }
}
