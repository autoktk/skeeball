﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Events;
using UnityEngine.UI;

namespace Controllers
{
    [CustomEditor(typeof(Bucket))]
    public class BucketEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                serializedObject.Update();
                EditorGUILayout.Space();
                GUILayout.Label("Editor", EditorStyles.boldLabel);
                var bucket = target as Bucket;

                int newScore = EditorGUILayout.IntField("Score", bucket.score);
                if (newScore != bucket.score)
                {
                    bucket.score = newScore;
                    var labelScoreProperty = serializedObject.FindProperty("labelScore");
                    EditorUtility.SetDirty(labelScoreProperty.objectReferenceValue);
                }
                Vector3 localScale = bucket.transform.localScale;
                float scale = EditorGUILayout.Slider("Scale", localScale.x, 0.01f, 20f);
                if (scale != bucket.transform.localScale.x)
                {
                    localScale.x = localScale.z = scale;
                    bucket.transform.localScale = localScale;
                    var scaler = bucket.GetComponentInChildren<CanvasScaler>();
                    var scalerLocalScale = scaler.transform.localScale;
                    scalerLocalScale.x = localScale.y / localScale.x / scaler.referencePixelsPerUnit;
                    scalerLocalScale.z = localScale.y / localScale.z / scaler.referencePixelsPerUnit;
                    scaler.transform.localScale = scalerLocalScale;
                    EditorUtility.SetDirty(bucket);
                    EditorUtility.SetDirty(scaler);
                }
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}



