﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Events;

namespace Controllers
{
    [CustomEditor(typeof(Game))]
    public class GameEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                serializedObject.Update();
                EditorGUILayout.Space();
                GUILayout.Label("Editor", EditorStyles.boldLabel);
                if (GUILayout.Button("Find Buckets"))
                {
                    Game gameController = (Game)target;
                    SerializedProperty bucketsProperty = serializedObject.FindProperty("buckets");
                    bucketsProperty.ClearArray();
                    var buckets = FindObjectsOfType<Bucket>();
                    bucketsProperty.arraySize = buckets.Length;


                    for (int i = 0; i < buckets.Length; i++)
                    {
                        var childProperty = bucketsProperty.GetArrayElementAtIndex(i);
                        EditorUtility.SetDirty(buckets[i]);
                        UnityEventTools.RemovePersistentListener<Bucket, Ball>(buckets[i].OnDetectBall, gameController.OnBucketDetectBall);
                        UnityEventTools.AddPersistentListener<Bucket, Ball>(buckets[i].OnDetectBall, gameController.OnBucketDetectBall);
                        childProperty.objectReferenceValue = buckets[i];
                    }
                }
                serializedObject.ApplyModifiedProperties();
            }
        }
    }
}



