﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Views
{
    public class Leaderboard : View
    {
        [Header("Setup")]
        [SerializeField]
        protected RectTransform panelItems;

        [Header("Resources")]
        [SerializeField]
        protected LeaderboardItem prefabItem;

        private void OnEnable()
        {
            var listRank = Datas.Leaderboard.GetRanks();
            for (int i = 0; i < listRank.Count; i++)
            {
                var item = Instantiate<LeaderboardItem>(prefabItem, panelItems, false);
                item.id = i + 1;
                item.score = listRank[i];
            }
        }

        private void OnDisable()
        {
            var items = panelItems.GetComponentsInChildren<LeaderboardItem>();
            for (int i=items.Length-1; i>=0; i--)
            {
                Destroy(items[i].gameObject);
            }
        }
    }
}
