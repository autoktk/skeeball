﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Views
{
    public class LeaderboardWithPosition : Leaderboard
    {
        [SerializeField]
        protected LeaderboardItem prefabItemHighlight;

        [HideInInspector]
        public int position;

        private void OnEnable()
        {
            var listRank = Datas.Leaderboard.GetRanks();
            for (int i = 0; i < listRank.Count; i++)
            {
                int id = i + 1;
                LeaderboardItem item = null;
                if (id == position)
                {
                    item = Instantiate<LeaderboardItem>(prefabItemHighlight, panelItems, false);
                }
                else
                {
                    item = Instantiate<LeaderboardItem>(prefabItem, panelItems, false);
                }
                item.id = id;
                item.score = listRank[i];
            }
        }
    }
}
