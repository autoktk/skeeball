﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Views
{
    public class Game : View
    {
        [Header("Setup")]
        [SerializeField]
        protected Slider sliderPower;
        [SerializeField]
        protected Text labelCountDown;
        [SerializeField]
        protected Text labelGameTime;
        [SerializeField]
        protected Text labelScore;
        [SerializeField]
        protected GameObject panelGameOver;
        [SerializeField]
        protected LeaderboardWithPosition leaderboard;
        [SerializeField]
        protected Text labelGameOverScore;
        [SerializeField]
        protected Text labelComment;
        [SerializeField]
        protected Button buttonTryAgain;

        [Header("Event")]
        [HideInInspector]
        public GameEvent OnTouchDown;
        [HideInInspector]
        public GameEvent OnTouchUp;
        [HideInInspector]
        public GameEvent OnTouchDrag;
        [HideInInspector]
        public UnityEvent OnTryAgainClicked;
        [HideInInspector]
        public UnityEvent OnGoBackClicked;

        [Header("Private")]
        private int pointerId;

        private void OnEnable()
        {
            pointerId = -1;
            sliderPower.gameObject.SetActive(false);
            panelGameOver.SetActive(false);
            labelCountDown.text = string.Empty;
        }

        public void UiOnPointerDown(BaseEventData baseEventData)
        {
            PointerEventData pointerEventData = (PointerEventData)baseEventData;
            if (pointerId < 0)
            {
                pointerId = pointerEventData.pointerId;
                OnTouchDown.Invoke(Camera.main.ScreenPointToRay(pointerEventData.pointerCurrentRaycast.screenPosition));
            }
        }

        public void UiOnPointerDrag(BaseEventData baseEventData)
        {
            PointerEventData pointerEventData = (PointerEventData)baseEventData;
            if (pointerId == pointerEventData.pointerId)
            {
                OnTouchDrag.Invoke(Camera.main.ScreenPointToRay(pointerEventData.pointerCurrentRaycast.screenPosition));
            }
        }

        public void UiOnPointerUp(BaseEventData baseEventData)
        {
            PointerEventData pointerEventData = (PointerEventData)baseEventData;
            if (pointerId == pointerEventData.pointerId)
            {
                OnTouchUp.Invoke(Camera.main.ScreenPointToRay(pointerEventData.pointerCurrentRaycast.screenPosition));
                pointerId = -1;
            }
        }

        public void UiOnTryAgainClicked()
        {
            OnTryAgainClicked.Invoke();
        }

        public void UiOnGoBackClicked()
        {
            OnGoBackClicked.Invoke();
        }

        public void SetGameTime(float time)
        {
            labelGameTime.text = string.Format("{0:0.00}s", time);
        }

        public void ShowCountDown(float count)
        {
            if (!labelCountDown.gameObject.activeSelf)
            {
                labelCountDown.gameObject.SetActive(true);
            }
            labelCountDown.text = Mathf.CeilToInt(count).ToString();
        }

        public void UnshowCountDown()
        {
            labelCountDown.gameObject.SetActive(false);
        }

        public void ShowPower(float powerPercentage)
        {
            if (!sliderPower.gameObject.activeSelf)
            {
                sliderPower.gameObject.SetActive(true);
            }
            sliderPower.value = (sliderPower.maxValue - sliderPower.minValue) * powerPercentage + sliderPower.minValue;
        }

        public void UnshowPower()
        {
            sliderPower.gameObject.SetActive(false);
        }

        public void SetScore(int score)
        {
            labelScore.text = score.ToString();
        }

        public void ClearScore()
        {
            labelScore.text = "0";
        }

        public void ShowGameOver(int score, int position)
        {
            labelGameOverScore.text = score.ToString();
            leaderboard.position = position;
            if (position == 1)
            {
                labelComment.text = Datas.Constant.COMMENT_TOP;
                buttonTryAgain.gameObject.SetActive(false);
            }
            else
            {
                labelComment.text = Datas.Constant.COMMENT_REST;
                buttonTryAgain.gameObject.SetActive(true);
            }
            panelGameOver.SetActive(true);
        }
    }

    [System.Serializable]
    public class GameEvent : UnityEvent<Ray>
    {
    }
}
