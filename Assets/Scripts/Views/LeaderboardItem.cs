﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class LeaderboardItem: LeaderboardObject
    {
        public int id
        {
            get 
            {
                if (int.TryParse(labelId.text, out int value))
                {
                    return value;
                }
                return 0;
            }
            set
            {
                labelId.text = value.ToString();
            }
        }

        public int score
        {
            get
            {
                if (int.TryParse(labelScore.text, out int value))
                {
                    return value;
                }
                return 0;
            }
            set
            {
                labelScore.text = value.ToString();
            }
        }
    }
}
