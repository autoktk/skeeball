﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Views
{
    public class Menu : View
    {
        [Header("Setup")]
        [SerializeField]
        protected GameObject panelLoading;

        [Header("Event")]
        [HideInInspector]
        public UnityEvent OnStartClicked;

        public void ShowLoading(bool show)
        {
            panelLoading.SetActive(show);
        }

        public void UiOnStartClicked()
        {
            OnStartClicked.Invoke();
        }
    }
}
