﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Views
{
    public class View : MonoBehaviour
    {
        [Header("Popup")]
        [SerializeField]
        private bool initShow = false;
        protected virtual void Awake()
        {
            gameObject.SetActive(initShow);
        }

        public void Show(bool show)
        {
            gameObject.SetActive(show);
        }
    }
}
