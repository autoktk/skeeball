﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class LeaderboardHeader : LeaderboardObject
    {
        [Header("Header")]
        [SerializeField]
        protected string fieldId;
        [SerializeField]
        protected string fieldScore;

        private void Awake()
        {
            labelId.text = fieldId;
            labelScore.text = fieldScore;
        }
    }
}
