﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Views
{
    public class LeaderboardObject : MonoBehaviour
    {
        [SerializeField]
        protected Text labelId;
        [SerializeField]
        protected Text labelScore;
    }
}
