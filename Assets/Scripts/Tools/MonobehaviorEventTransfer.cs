﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Tools
{
    public class MonobehaviorEventTransfer : MonoBehaviour
    {
        public MonobehaviorEventTransferBasicEvent AwakeTransfer;
        public MonobehaviorEventTransferBasicEvent StartTransfer;
        public MonobehaviorEventTransferBasicEvent OnEnableTransfer;
        public MonobehaviorEventTransferBasicEvent OnDisableTransfer;
        public MonobehaviorEventTransferBasicEvent UpdateTransfer;
        public MonobehaviorEventTransferBasicEvent LateUpdateTransfer;
        public MonobehaviorEventTransferColliderEvent OnTriggerEnterTransfer;
        public MonobehaviorEventTransferColliderEvent OnTriggerStayTransfer;
        public MonobehaviorEventTransferColliderEvent OnTriggerExitTransfer;
        public MonobehaviorEventTransferCollisionEvent OnCollisionEnterTransfer;
        public MonobehaviorEventTransferCollisionEvent OnCollisionStayTransfer;
        public MonobehaviorEventTransferCollisionEvent OnCollisionExitTransfer;


        private void Awake()
        {
            AwakeTransfer.Invoke(this);
        }

        private void OnEnable()
        {
            OnEnableTransfer.Invoke(this);
        }

        private void OnDisable()
        {
            OnDisableTransfer.Invoke(this);
        }

        private void Start()
        {
            StartTransfer.Invoke(this);
        }

        private void Update()
        {
            UpdateTransfer.Invoke(this);
        }

        private void LateUpdate()
        {
            LateUpdateTransfer.Invoke(this);
        }

        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEnterTransfer.Invoke(this, other);
        }

        private void OnTriggerStay(Collider other)
        {
            OnTriggerStayTransfer.Invoke(this, other);
        }

        private void OnTriggerExit(Collider other)
        {
            OnTriggerExitTransfer.Invoke(this, other);
        }

        private void OnCollisionEnter(Collision collision)
        {
            OnCollisionEnterTransfer.Invoke(this, collision);
        }

        private void OnCollisionStay(Collision collision)
        {
            OnCollisionStayTransfer.Invoke(this, collision);
        }

        private void OnCollisionExit(Collision collision)
        {
            OnCollisionExitTransfer.Invoke(this, collision);
        }
    }

    [System.Serializable]
    public class MonobehaviorEventTransferBasicEvent : UnityEvent<MonobehaviorEventTransfer>
    {
    }
    [System.Serializable]
    public class MonobehaviorEventTransferColliderEvent : UnityEvent<MonobehaviorEventTransfer, Collider>
    {
    }
    [System.Serializable]
    public class MonobehaviorEventTransferCollisionEvent : UnityEvent<MonobehaviorEventTransfer, Collision>
    {
    }
}
