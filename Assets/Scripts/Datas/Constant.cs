﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Datas
{
    public static class Constant
    {
        public const string KEY_LEADERBOARD = "key_leaderboard";
        public const string LAYER_NAME_SHOOTER = "Shooter";

        public const int SCORE_MULTIPLER_POOL_SIZE = 20;
        public const int SCORE_MULTIPLER_POOL_X2 = 5;

        public const string COMMENT_TOP = "Amazing! You got the hightest score!";
        public const string COMMENT_REST = "Your score is low. Try again";

        public const string GAME_SCENE_1 = "Game_1";
    }
}
