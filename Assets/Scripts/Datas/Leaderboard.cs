﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Datas
{
    public static class Leaderboard
    {
        private const int NUMBER_OF_RECORD = 5;

        public static List<int> GetRanks()
        {
            string rankData = PlayerPrefs.GetString(Constant.KEY_LEADERBOARD, "");
            string[] ranks = rankData.Split(',');
            List<int> listRank = new List<int>();
            foreach (var rank in ranks)
            {
                if (int.TryParse(rank, out int rankInt))
                    listRank.Add(rankInt);
            }
            while (listRank.Count < NUMBER_OF_RECORD)
            {
                listRank.Add(0);
            }
            return listRank;
        }

        /**
         * @desc add new score to ranking list. if score already exists, do not add and return the rank with the same score
         * @return float: the ranking of the input score
         *      0: the new score not in first n ranking
         *      else: in first n ranking
         *      which n <= NUMBER_OF_RECORD
         */
        public static int AddScore(int score)
        {
            List<int> listRank = GetRanks();
            if (listRank.Contains(score))
            {
                return listRank.FindIndex((item) => item == score) + 1;
            }
            else
            {
                listRank.Add(score);
                listRank.Sort((a, b) => -a.CompareTo(b));
                listRank.RemoveAt(NUMBER_OF_RECORD);
                PlayerPrefs.SetString(Constant.KEY_LEADERBOARD, string.Join(",", listRank));
                PlayerPrefs.Save();
                return listRank.FindIndex((item) => item == score) + 1;
            }
        }
    }
}
